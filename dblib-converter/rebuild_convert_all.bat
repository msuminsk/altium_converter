REM In Windows Command Prompt (not MSYS):

REM one-time: pip install -U pyinstaller

rmdir /q /s dist build
del convert_all.spec

pyinstaller.exe --onefile convert_all.py
copy /y dist\convert_all.exe .